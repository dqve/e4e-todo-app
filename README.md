 
# vuestate project 
>  ehealth4everyone todo assignment

### project dependencies

>   "vue": "^2.5.21" (VueJs), 
    "vue-router": "^3.0.1",
    "vuex": "^3.0.1" (Vuex-store), 
    "vuex-persist": "^2.0.0" (Vuex-persist to store vuex states)
    
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).